package ca.sprint4.myfunds.controller;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Set;

import ca.sprint4.myfunds.repositories.*;
import ca.sprint4.myfunds.dto.*;
import ca.sprint4.myfunds.model.*;
import ca.sprint4.myfunds.services.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "*")
@RestController
public class UserController {
	@Autowired
	MyFundsServices services;
	
    @Autowired
    UserRepository userRepository;
    
    @Autowired
    BudgetAllocationRepository budgetAllocationRepository;
    
    @GetMapping("/budget")
    public List<BudgetAllocationDto> viewBudgetAllocation(@RequestParam(name = "budgetid") int budgetID) {
    	Set<BudgetAllocation> budgetAllocations = services.getBudgetAllocationBreakdown(budgetID);
    	List<BudgetAllocationDto> dtos = new ArrayList<BudgetAllocationDto>();
    	
    	for(BudgetAllocation allocation : budgetAllocations) {
    		dtos.add(new BudgetAllocationDto(allocation.getCategory(), allocation.getAmountAllocated(), 
        			allocation.getAmountLeftToSpend(), allocation.getAmountAllocated() - allocation.getAmountLeftToSpend()));
    	}
    	
    	return dtos;
    }

    @PostMapping("/budget/allocate")
    public BudgetAllocationDto createBudgetAllocation(@RequestParam(name = "budgetid") int budgetID, @RequestParam(name = "category") String category,
    		@RequestParam(name = "amount") double amount) {
    	BudgetAllocation newAllocation = services.createBudgetAllocation(budgetID, amount, category);
    	
    	BudgetAllocationDto dto = new BudgetAllocationDto(newAllocation.getCategory(), newAllocation.getAmountAllocated(), 
    			newAllocation.getAmountLeftToSpend(), newAllocation.getAmountAllocated() - newAllocation.getAmountLeftToSpend());
    	
    	return dto;
    }
    
    @PostMapping("/budget/create")
    public int createBudget(@RequestParam(name = "accountnumber") int accountNumber) {
    	Budget budget = services.createBudget(accountNumber);
    	
    	return budget.getOwner().getAccountNumber();
    }
    
    @PostMapping("/account/create")
    public int createAccount(@RequestParam(name = "ownerusername") String username, @RequestParam(name = "accountnum") int accountNumber) {
    	Account account = services.createAccount(username, accountNumber);
    	
    	return account.getAccountNumber();
    }
    
    @PostMapping("/user/signup")
    public String createUser(@RequestParam(name = "username") String username, @RequestParam(name = "password") String password) {
    	Users user = services.createUser(username, password);
    	
    	return user.getUsername() + " " + user.getPassword();
    }
    
    @PostMapping("/transaction")
    public void createUser(@RequestParam(name = "accountnumber") int accountNumber, @RequestParam(name = "amount") double amount,
    		@RequestParam(name = "vendor") String vendor, @RequestParam(name = "category") String category) {
    	services.createTransaction(accountNumber, amount, vendor, category);
    	return;
    }
    
    @GetMapping("/transaction")
    public List<TransactionDto> viewTransactions(@RequestParam(name = "accountnumber") int accountNumber) {
    	List<Transaction> transactions = services.getAllTransactionsByAccount(accountNumber);
    	List<TransactionDto> dtos = new ArrayList<TransactionDto>();
    	
    	for(Transaction t : transactions) {
    		dtos.add(new TransactionDto(t.getAmount(), t.getDate(), t.getCategory(), t.getVendor()));
    	}
    	
    	return dtos;
    }
}