package ca.sprint4.myfunds;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyFundsApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyFundsApplication.class, args);
	}

}
