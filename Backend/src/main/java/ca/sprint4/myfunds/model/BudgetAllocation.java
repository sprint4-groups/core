package ca.sprint4.myfunds.model;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "budgetallocation")
public class BudgetAllocation {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int budgetAllocationID;
	
	private String category;
	
	private double amountAllocated;
	
	private double amountLeftToSpend;
	
	public double getAmountLeftToSpend() {
		return amountLeftToSpend;
	}

	public void setAmountLeftToSpend(double amountLeftToSpend) {
		this.amountLeftToSpend = amountLeftToSpend;
	}

	@ManyToOne
	private Budget budget;

	public int getBudgetAllocationID() {
		return budgetAllocationID;
	}

	public void setBudgetAllocationID(int budgetAllocationID) {
		this.budgetAllocationID = budgetAllocationID;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public double getAmountAllocated() {
		return amountAllocated;
	}

	public void setAmountAllocated(double amountAllocated) {
		this.amountAllocated = amountAllocated;
	}

	public Budget getBudget() {
		return budget;
	}

	public void setBudget(Budget budget) {
		this.budget = budget;
	}
}
