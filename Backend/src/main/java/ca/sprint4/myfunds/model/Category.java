package ca.sprint4.myfunds.model;

public enum Category {

    Groceries, Electronics, Clothing, Education, Transportation,
    Mortgage, Entertainment, Furniture, Medical, Expenses, TVPhoneInternet, Other;
}
