package ca.sprint4.myfunds.model;

import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "budget")
public class Budget {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int budgetID;
	
	private double weeklyBudgetAmount;

	private double monthlyBudgetAmount;
	
	private double yearlyBudgetAmount;
	
	@OneToMany
	private Set<BudgetAllocation> allocation;
	
	@OneToOne
	private Account owner;

	public int getBudgetID() {
		return budgetID;
	}

	public void setBudgetID(int budgetID) {
		this.budgetID = budgetID;
	}

	public double getWeeklyBudgetAmount() {
		return weeklyBudgetAmount;
	}

	public void setWeeklyBudgetAmount(double weeklyBudgetAmount) {
		this.weeklyBudgetAmount = weeklyBudgetAmount;
	}

	public double getMonthlyBudgetAmount() {
		return monthlyBudgetAmount;
	}

	public void setMonthlyBudgetAmount(double monthlyBudgetAmount) {
		this.monthlyBudgetAmount = monthlyBudgetAmount;
	}

	public double getYearlyBudgetAmount() {
		return yearlyBudgetAmount;
	}

	public void setYearlyBudgetAmount(double yearlyBudgetAmount) {
		this.yearlyBudgetAmount = yearlyBudgetAmount;
	}

	public Set<BudgetAllocation> getAllocation() {
		return allocation;
	}

	public void setAllocation(Set<BudgetAllocation> allocation) {
		this.allocation = allocation;
	}
	
	public Account getOwner() {
		return owner;
	}

	public void setOwner(Account owner) {
		this.owner = owner;
	}
}
