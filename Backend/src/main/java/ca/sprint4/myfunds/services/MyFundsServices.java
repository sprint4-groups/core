package ca.sprint4.myfunds.services;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ca.sprint4.myfunds.model.*;
import ca.sprint4.myfunds.repositories.*;

@Service
public class MyFundsServices {
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private AccountRepository accountRepository;
	
	@Autowired
	private TransactionRepository transactionRepository;
	
	@Autowired
	private BudgetRepository budgetRepository;
	
	@Autowired
	private BudgetAllocationRepository budgetAllocationRepository;
	
	//write services here
	@Transactional
	public Users createUser(String username, String password) {
		Users newUser = new Users();
		newUser.setPassword(password);
		newUser.setUsername(username);
		
		return userRepository.save(newUser);
	}
	
	@Transactional
	public Account createAccount(String ownerUsername, int accountNumber) {
		Users owner;
		try {
			owner = userRepository.findByUsername(ownerUsername);
		}
		catch(Exception e) {
			throw new IllegalArgumentException("Invalid username to add account for");
		}
		
		if(owner != null) {
			Account newAccount = new Account();
			
			newAccount.setOwner(owner);
			newAccount.setAccountNumber(accountNumber);
			newAccount.setBalance(0);
			
			Set<Account> accounts = owner.getAccounts();
			accounts.add(newAccount);
			owner.setAccounts(accounts);
			
			newAccount = accountRepository.save(newAccount);
			userRepository.save(owner);
			return newAccount;
		}
		else {
			return null;
		}
	}
	
	@Transactional
	public Budget createBudget(int accountNumber) {
		Account owner;
		try {
			owner = accountRepository.findByAccountNumber(accountNumber);
		}
		catch(Exception e) {
			throw new IllegalArgumentException("Invalid account number to create budget for");
		}
		
		if(owner != null) {
			Budget newBudget = new Budget();
			newBudget.setMonthlyBudgetAmount(0);
			newBudget.setWeeklyBudgetAmount(0);
			newBudget.setYearlyBudgetAmount(0);
			newBudget.setOwner(owner);
			
			owner.setBudget(newBudget);
			
			accountRepository.save(owner);
			return budgetRepository.save(newBudget);
		}
		else {
			return null;
		}
	}
	
	@Transactional
	public BudgetAllocation createBudgetAllocation(int budgetID, double amount, String category) {
		Budget budget;
		try {
			budget = budgetRepository.findByBudgetID(budgetID);
		}
		catch(Exception e) {
			throw new IllegalArgumentException("Invalid budget ID");
		}
		
		if(budget != null) {
			BudgetAllocation newBudgetAllocation = new BudgetAllocation();
			
			double budgetLeft = amount;
			
			List<Transaction> transactions = transactionRepository.findAll();
			
			for(Transaction t : transactions) {
				if(t.getCategory().equals(category)) {
					budgetLeft = budgetLeft - t.getAmount();
				}
			}
			
			newBudgetAllocation.setAmountAllocated(amount);
			newBudgetAllocation.setBudget(budget);
			newBudgetAllocation.setCategory(category);
			newBudgetAllocation.setAmountLeftToSpend(budgetLeft);
			
			Set<BudgetAllocation> allocation = budget.getAllocation();
			allocation.add(newBudgetAllocation);
			budget.setAllocation(allocation);
			budget.setMonthlyBudgetAmount(budget.getMonthlyBudgetAmount() + amount);
			
			newBudgetAllocation = budgetAllocationRepository.save(newBudgetAllocation);
			budgetRepository.save(budget);
			return newBudgetAllocation;
		}
		else {
			return null;
		}
	}
	
	@Transactional
	public Transaction createTransaction(int accountNumber, double amount, String vendor, String category) {
		Account account;
		try {
			account = accountRepository.findByAccountNumber(accountNumber);
		}
		catch(Exception e) {
			throw new IllegalArgumentException("Invalid account number to make transaction from");
		}
		
		if(account != null) {
			Transaction newTransaction = new Transaction();
			
			newTransaction.setAccount(account);
			newTransaction.setDate(new java.sql.Date(Calendar.getInstance().getTime().getTime()));
			newTransaction.setAmount(amount);
			newTransaction.setVendor(vendor);
			newTransaction.setCategory(category);
			
			Set<BudgetAllocation> allocations = budgetAllocationRepository.findByCategory(category);
			
			for(BudgetAllocation a : allocations) {
				if(a.getCategory().equals(category) && a.getBudget().getOwner().getAccountNumber() == accountNumber) {
					a.setAmountLeftToSpend(a.getAmountLeftToSpend() - amount);
					budgetAllocationRepository.save(a);
					break;
				}
			}
			
			return transactionRepository.save(newTransaction);
		}
		else {
			return null;
		}
	}
	
	@Transactional
	public List<Transaction> getAllTransactionsByAccount(int accountNumber) {
		Account account;
		try {
			account = accountRepository.findByAccountNumber(accountNumber);
		}
		catch(Exception e) {
			throw new IllegalArgumentException("Invalid account number to make transaction from");
		}
		
		if(account != null) {
			List<Transaction> allTransactions = transactionRepository.findAll();
			List<Transaction> result = new ArrayList<Transaction>();
			
			for(Transaction t : allTransactions) {
				if(t.getAccount().getAccountNumber() == accountNumber) {
					result.add(t);
				}
			}
			
			return result;
		}
		else {
			return null;
		}
	}
	
	@Transactional
	public Set<BudgetAllocation> getBudgetAllocationBreakdown(int budgetID) {
		Budget budget;
		try {
			budget = budgetRepository.findByBudgetID(budgetID);
		}
		catch(Exception e) {
			throw new IllegalArgumentException("Invalid budget ID");
		}
		
		if(budget != null) {
			return budget.getAllocation();
		}
		else {
			return null;
		}
	}
}
