package ca.sprint4.myfunds.repositories;

import java.util.Set;

import org.springframework.data.jpa.repository.JpaRepository;

import ca.sprint4.myfunds.model.BudgetAllocation;

public interface BudgetAllocationRepository extends JpaRepository<BudgetAllocation, Integer> {
	public Set<BudgetAllocation> findByCategory(String category);
}
