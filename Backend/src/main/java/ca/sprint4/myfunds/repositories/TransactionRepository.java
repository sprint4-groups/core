package ca.sprint4.myfunds.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ca.sprint4.myfunds.model.Transaction;

public interface TransactionRepository extends JpaRepository<Transaction, Integer> {

}
