package ca.sprint4.myfunds.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ca.sprint4.myfunds.model.Budget;

public interface BudgetRepository extends JpaRepository<Budget, Integer> {
	public Budget findByBudgetID(int budgetID);
}
