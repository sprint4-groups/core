package ca.sprint4.myfunds.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ca.sprint4.myfunds.model.Users;

public interface UserRepository extends JpaRepository<Users, String> {
	public Users findByUsername(String username);
}
