package ca.sprint4.myfunds.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ca.sprint4.myfunds.model.Account;

public interface AccountRepository extends JpaRepository<Account, Integer> {
	public Account findByAccountNumber(int accountNumber);
}
