package ca.sprint4.myfunds.dto;

public class BudgetAllocationDto {
	private String category;
	
	private double amountAllocated;
	
	private double amountLeftToSpend;
	
	public BudgetAllocationDto(String category, double amountAllocated,
			double amountLeftToSpend, double amountSpent) {
		super();
		this.category = category;
		this.amountAllocated = amountAllocated;
		this.amountLeftToSpend = amountLeftToSpend;
		this.amountSpent = amountSpent;
	}

	private double amountSpent;

	public double getAmountLeftToSpend() {
		return amountLeftToSpend;
	}

	public void setAmountLeftToSpend(double amountLeftToSpend) {
		this.amountLeftToSpend = amountLeftToSpend;
	}

	public double getAmountSpent() {
		return amountSpent;
	}

	public void setAmountSpent(double amountSpent) {
		this.amountSpent = amountSpent;
	}

	public BudgetAllocationDto(String category, double amountAllocated) {
		super();
		this.category = category;
		this.amountAllocated = amountAllocated;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public double getAmountAllocated() {
		return amountAllocated;
	}

	public void setAmountAllocated(double amountAllocated) {
		this.amountAllocated = amountAllocated;
	}
	
}
