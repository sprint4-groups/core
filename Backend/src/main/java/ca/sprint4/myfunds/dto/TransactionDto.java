package ca.sprint4.myfunds.dto;

import java.sql.Date;

public class TransactionDto {
	private double amount;
	
	private Date date;
	
	private String category;
	
	private String vendor;

	public TransactionDto(double amount, Date date, String category, String vendor) {
		super();
		this.amount = amount;
		this.date = date;
		this.category = category;
		this.vendor = vendor;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getVendor() {
		return vendor;
	}

	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
}
