package ca.sprint4.myfunds.services;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import ca.sprint4.myfunds.model.*;
import ca.sprint4.myfunds.repositories.UserRepository;

@SpringBootTest
public class ServicesTest {

	@Autowired
	private MyFundsServices services;
	
	@Test
    public void testCreateUser() {
        Users user = services.createUser("username", "123");
        assertEquals(user.getUsername(), "username");
        assertEquals(user.getPassword(), "123");
    }
}
