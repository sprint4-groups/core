function ClearFields() {

  document.getElementById("Amount").value = "";
  document.getElementById("Period").value = "";
}

function LoadChart() {

google.charts.load('current', {
  'packages': ['corechart']
});
google.charts.setOnLoadCallback(drawChart);
}


function drawChart() {

  var data = google.visualization.arrayToDataTable([
    ['Category', 'Dollars per Month'],
    ['Groceries', 100],
    ['Electronics', 250],
    ['Clothing', 200],
    ['Education', 600],
    ['Transport', 50]
  ]);

  var options = {};

  var chart = new google.visualization.PieChart(document.getElementById('piechart'));

  chart.draw(data, options);
}

document.addEventListener('DOMContentLoaded', function() {
  var elems = document.querySelectorAll('select');
  var instances = M.FormSelect.init(elems, options);
});

//API
var request = new XMLHttpRequest()
request.open('GET', 'https://ghibliapi.herokuapp.com/films')
request.onload = function() {
  var data = JSON.parse(this.response)
  if (request.status >= 200 && request.status < 400) {
    data.forEach(movie => {
      console.log(movie.title)
    })
  } else {
    console.log('error')
  }
}
request.send()
